
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Form</title>
    <link rel="stylesheet" href="./assets/style.css">
    <link rel="icon" href="icon.jpg">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>    
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
</head>
<body>
    <div id="background-carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active" style="background-image:url(./assets/img/12.jpg)"></div>
                <!-- <div class="item" style="background-image:url(./assets/img/22.jpg)"></div> -->
                <!-- <div class="item" style="background-image:url(./assets/img/32.jpg)"></div> -->
                <div class="item" style="background-image:url(./assets/img/4.jpg)"></div>
                <div class="item" style="background-image:url(./assets/img/5.jpg)"></div>
                <!-- <div class="item" style="background-image:url(./assets/img/6.jpg)"></div> -->
                <!-- <div class="item" style="background-image:url(./assets/img/7.jpg)"></div> -->
                <!-- <div class="item" style="background-image:url(./assets/img/8.jpg)"></div> -->
                <div class="item" style="background-image:url(./assets/img/9.jpg)"></div>
                <!-- <div class="item" style="background-image:url(./assets/10img.jpg)"></div> -->
                <div class="item" style="background-image:url(./assets/11.jpg)"></div>
                <!-- <div class="item" style="background-image:url(./assets/img/13.jpg)"></div> -->
                <!-- <div class="item" style="background-image:url(./assets/img/14.jpg)"></div> -->
            </div>  
        </div>
    </div>

    <div id="content-wrapper">
        <div class="title"><h1></h1></div>
        <br><br>
        <div class="content">
            <div class="left"></div>
            <div class="right">
                <div class="contentContainer">
                    <div class="sign"><h1>sign in</h1></div>
                        <form action="#">
                            <div class="inputBox">
                                <input type="text" name="username" autocomplete="off" required>
                                <i class="material-icons">person</i><label>Username</label>
                            </div>
                            <div class="inputBox">
                                <input type="password" name="password" required>
                                    <i class="material-icons">lock</i><label>Password</label>
                            </div>
                        <button name="submit" value="SIGN IN">SIGN IN</button>
                        <p><br>Doesn't have an account? <a href="#">Sign Up</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<script>
    $('#myCarousel').carousel({
        pause: 'none'
    })
</script>