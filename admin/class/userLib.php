<?php

    class Lib {
        public $Connect;

        public function __construct() {
            require_once("../../config/connect.php");
            $this -> Connect = new Connect();
        }

        public function add($arg) {
            try {
                $sql = "INSERT INTO user(username, password, fullname) VALUES (?,?,?)";

                $insert = $this -> Connect -> db -> prepare ($sql);
                $insert -> bindparam(1, $arg[0]);
                $insert -> bindparam(1, $arg[1]);
                $insert -> bindparam(1, $arg[2]);
                $insert -> execute();
                return true;
            } catch (PDOException $e) {
                return "Failed";
            }
        }

        public function edit($code) {
            try {
                $sql = "SELECT * FROM user WHERE id = ?";
                
                $appear = $this -> Connect -> db -> prepare ($sql);
                $appear -> bindparam(1, $code[0]);
                $appear -> execute();
                return true;
            } catch (PDOException $e) {
                return false;
            }
        }

        public function update($code, $username, $password, $fullname) {
            try {
                $sql = "UPDATE user SET username = ?,
                                        password = ?,
                                        fullname = ?
                                WHERE id = ?";

                $update = $this -> Connect -> db -> prepare ($sql);
                $update -> bindparam(1, $code);
                $update -> bindparam(2, $username);
                $update -> bindparam(3, $password);
                $update -> bindparam(4, $fullname);
                $update -> execute();
                return true;
            } catch (PDOException $e) {
                return false;
            }
        }

        public function read() {
            try {
                $sql = "SELECT * FROM user";
                $query = $this -> Connect -> db -> query ($sql);
                return $query;
            } catch (PDOException $e) {
                return false;
            }
        }

        public function delete($code) {
            try {
                $sql = "DELETE FROM user WHERE id = ?";

                $delete = $this -> Connect -> db -> prepare ($sql);
                $delete -> bindparam(1, $code);
                $delete -> execute();
                return true;
            } catch (PDOException $e) {
                return false;
            }
        }

    }