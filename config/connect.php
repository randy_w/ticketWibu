<?php

    class Connect {
        private $host = "localhost",
                $user = "root",
                $pass = "",
                $db_name = "db_praukom1_xiirpla1819_29_randywardhana";
        public $db;

        function __construct(){
            try {
                $this -> db = new PDO("mysql:host={$this -> host};
                                        dbname={$this -> db_name}",
                                        $this -> user,
                                        $this -> pass);
                $this -> db -> setAttribute(PDO::ATTR_ERRMODE,
                                            PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $e) {
                die("Connection error : ".$e -> getMessage());
            }
        }

    }

    $Connect = new Connect();